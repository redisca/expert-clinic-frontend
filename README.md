# What's used
- `Webpacker` gem to handle webpack assets bundling

## Install

##### MacOS

```bash
curl -sSL https://get.rvm.io | bash -s stable --rails
bundle install
yarn install
```

## Dev run
- `rails s` in order to start rails app, `localhost:3000`
- `./bin/webpack-dev-server` to run webpack dev server with hot reloading

## Deploy
`cap production deploy`

## File structure
```
app
  assets
    images
    javascripts
    stylesheets
  javascript
    packs - directory for Webpack entrypoints
      application.js 
```

## Gems used
- Webpacker
- React-Rails https://github.com/reactjs/react-rails
