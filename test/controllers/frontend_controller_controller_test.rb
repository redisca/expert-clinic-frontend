require 'test_helper'

class FrontendControllerControllerTest < ActionDispatch::IntegrationTest
  test "should get homepage" do
    get frontend_controller_homepage_url
    assert_response :success
  end

end
