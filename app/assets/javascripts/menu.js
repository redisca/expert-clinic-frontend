document.addEventListener("DOMContentLoaded", function () {
    const hamburgerButton = document.querySelector('.navigation-hamburger');
    const hamburgerCloseButton = document.querySelector('.menu__close');
    const menuContainer = document.querySelector('.menu');
    const html = document.querySelector('html');
    const body = document.querySelector('body');

    hamburgerButton.addEventListener('click', function () {
        menuContainer.classList.add('menu_visible');


        html.classList.add('no-scroll');
        body.classList.add('no-scroll');
    });

    hamburgerCloseButton.addEventListener('click', function () {
        menuContainer.classList.remove('menu_visible');

        html.classList.remove('no-scroll');
        body.classList.remove('no-scroll');
    });

    // Expand subitems
    const headers = document.querySelectorAll('.menu-column__header');
    headers.forEach(header => {
        header.addEventListener('click', function () {
            if (window.innerWidth >= 768) {
                return;
            }

            const items = header.parentNode.querySelector('.menu-items');
            if (items.classList.contains('menu-items_expanded')) {
                items.classList.remove('menu-items_expanded');
                header.classList.remove('menu-column__header_expanded');
                items.style['maxHeight'] = null;

            } else {
                items.classList.add('menu-items_expanded');
                header.classList.add('menu-column__header_expanded');
                items.style['maxHeight'] = items.scrollHeight + 'px';
            }
        })
    });

    window.addEventListener('resize', function () {
        if (window.innerWidth >= 768) {
            const expanded = document.querySelectorAll('.menu-items_expanded, .menu-column__header_expanded');

            expanded.forEach(element => {
                element.style['maxHeight'] = null;
                element.classList.remove('menu-items_expanded');
                element.classList.remove('menu-column__header_expanded');
            });
        }
    })
});