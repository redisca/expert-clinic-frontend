document.addEventListener("DOMContentLoaded", function () {
    const expandables = document.querySelectorAll('[data-expandable]');

    for (let i = 0; i < expandables.length; i++) {
        const expandable = expandables[i];

        const headerClass = expandable.getAttribute('data-expandable-header');
        const contentClass = expandable.getAttribute('data-expandable-content');
        const expandedClass = expandable.getAttribute('data-expandable');

        const header = expandable.querySelector('.' + headerClass);
        const content = expandable.querySelector('.' + contentClass);

        header.addEventListener('click', function (event) {
            if (expandable.classList.contains(expandedClass)) {
                expandable.classList.remove(expandedClass);
                content.style.maxHeight = 0;
            } else {
                expandable.classList.add(expandedClass);
                content.style.maxHeight = content.scrollHeight + 'px';
            }
            event.preventDefault();
        })
    }
});