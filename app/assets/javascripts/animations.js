import "waypoints/lib/noframework.waypoints.min";

document.addEventListener("DOMContentLoaded", function () {
    const waypoints = document.querySelectorAll('.animation-waypoint');

    for (let i = 0; i < waypoints.length; i++) {
        const waypoint = waypoints[i];

        new Waypoint({
            element: waypoint,
            offset: '95%',
            handler: function () {
                waypoint.classList.add("animation-trigger");
            }
        });
    }
});

window.addEventListener('load', function () {
    const elements = document.querySelectorAll('.preload');

    elements.forEach(el => {
        el.classList.remove('preload')

    });

});