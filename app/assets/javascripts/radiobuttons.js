document.addEventListener("DOMContentLoaded", function () {
    const radiobuttons = document.querySelectorAll('.radiobutton');

    radiobuttons.forEach(function (el) {
        const input = el.querySelector('.radiobutton__input');

        input.addEventListener('change', function () {
            el.classList.add('radiobutton_active');

            const currentName = input.getAttribute('name');

            const activeRadiobuttons = document.querySelectorAll('.radiobutton_active');
            const activeRadiobutton = document.querySelector('.radiobutton__input[name=' + currentName + ']:checked');

            if (!activeRadiobutton) {
                return;
            }

            activeRadiobuttons.forEach(function (activeRadio) {
                const control = activeRadio.querySelector('.radiobutton__input');

                if (control !== activeRadiobutton && control.getAttribute('name') === currentName) {
                    activeRadio.classList.remove('radiobutton_active');
                }
            })
        })
    })
});