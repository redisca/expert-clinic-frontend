document.addEventListener("DOMContentLoaded", function () {
    const popups = document.querySelectorAll('[data-popup]');

    const showEvent = document.createEvent('Event');
    showEvent.initEvent('show', true, true);

    for (let i = 0; i < popups.length; i++) {
        const popup = popups[i];

        popup.addEventListener('click', function (event) {
            const popupContainer = document.getElementById(popup.getAttribute('data-popup'));
            popupContainer.dispatchEvent(showEvent);
            event.preventDefault();
        })
    }
});