class FrontendController < ApplicationController
  def homepage
  end

  def doctors
  end

  def doctor_page
  end

  def services_adults
  end

  def services_diagnostics
  end

  def services_surgery
  end

  def service_mri
  end

  def service_cardiology
  end

  def service_page
  end

  def about_clients
  end

  def about_partners
  end

  def about_investors
  end

  def about_legal
  end

  def patients_notes
  end

  def patients_faq
  end

  def patients_legal
  end

  def promo_view
  end

  def city_confirm
  end

  def appointment_doctor
  end

  def appointment_doctor_success
  end
end
