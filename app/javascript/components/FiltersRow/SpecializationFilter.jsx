import React from "react";
import DropdownFilter from "../Filter/DropdownFilter";

const specializations = {
  0: "Все специализации",
  1: "Ведение беременности",
  2: "Гинекология",
  3: "Гирудотерапия",
  4: "Генетика",
  5: "Кардиология",
  6: "Хирургия"
};

const SpecializationFilter = () => (
  <DropdownFilter
    name="specialization"
    getTitle={value => specializations[value]}
    search
    title="Специализация"
    searchPlaceholder="Введите специализацию"
    values={specializations}
    defaultValue={0}
  />
);

export default SpecializationFilter;
