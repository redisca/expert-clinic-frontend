import React from "react";
import DropdownFilter from "./../Filter/DropdownFilter";
import CalendarFilter from "./../Filter/CalendarFilter";
import Sticky from "react-stickynode";
import { format } from "date-fns";
import { ru } from "date-fns/locale";
import SpecializationFilter from "./SpecializationFilter";

class FiltersRow extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      stickyEnabled: false
    };

    this.isStickyEnabled = this.isStickyEnabled.bind(this);
  }

  componentDidMount() {
    if (typeof window !== "undefined") {
      window.addEventListener("resize", this.isStickyEnabled);
      this.isStickyEnabled();
    }
  }

  componentWillUnmount() {
    if (typeof window !== "undefined") {
      window.removeEventListener("resize", this.isStickyEnabled);
    }
  }

  isStickyEnabled() {
    this.setState({ stickyEnabled: window.innerWidth >= 768 });
  }

  render() {
    const time = {
      0: "Любое",
      1: "с 8:00 до 11:00",
      2: "с 11:00 до 13:00",
      3: "с 13:00 до 15:00",
      4: "с 15:00 до 17:00",
      5: "с 17:00 до 19:00"
    };

    return (
      <>
        <Sticky
          enabled={this.state.stickyEnabled}
          innerZ={100}
          bottomBoundary={this.props.bottom}
        >
          <div className="filters doctors-page__filters">
            <div className="filter_specialization">
              <SpecializationFilter />
            </div>

            <div className="filter_day">
              <CalendarFilter
                name="day"
                title="Дата"
                getTitle={day => (
                  <>
                    <span className="day-of-week">
                      {format(day, "EEEE", { locale: ru })},&nbsp;
                    </span>
                    {format(day, "d MMMM", { locale: ru })}
                  </>
                )}
                defaultValue={new Date()}
              />
            </div>

            <div className="filter_time">
              <DropdownFilter
                name="time"
                getTitle={value => time[value]}
                title="Желаемое время"
                values={time}
                defaultValue={0}
              />
            </div>
          </div>
        </Sticky>
      </>
    );
  }
}

export default FiltersRow;
