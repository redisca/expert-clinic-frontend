import React, { useState, useRef, useEffect } from "react";
import CustomScroll from "react-custom-scroll";
import { CSSTransition } from "react-transition-group";
import classnames from "class-names";
import SearchIcon from "./../icons/SearchIcon";

const NavigationSearch = () => {
  const data = [
    {
      title: "Направления",
      data: [
        { title: "Кардиология", link: "#" },
        { title: "МРТ сосудов", link: "#" },
        { title: "МРТ головного мозга (турецкое седло)", link: "#" }
      ]
    },

    {
      title: "Врачи",
      data: [
        { title: "Алеев Равиль Зякирович", link: "#" },
        { title: "Александров Илья Викторович", link: "#" },
        { title: "Алексеева Ирина Владимировна", link: "#" },
        { title: "Алеев Равиль Зякирович", link: "#" },
        { title: "Александров Илья Викторович", link: "#" },
        { title: "Алексеева Ирина Владимировна", link: "#" },
        { title: "Алеев Равиль Зякирович", link: "#" },
        { title: "Александров Илья Викторович", link: "#" },
        { title: "Алексеева Ирина Владимировна", link: "#" },
        { title: "Алеев Равиль Зякирович", link: "#" },
        { title: "Александров Илья Викторович", link: "#" },
        { title: "Алексеева Ирина Владимировна", link: "#" }
      ]
    }
  ];
  const [dropdownShown, setDropdownShown] = useState(false);
  const [query, setQuery] = useState("");
  const searchRef = useRef(null);

  const handleChange = event => {
    const value = event.target.value;
    setQuery(value);
    setDropdownShown(value !== "");
  };

  const hideDropdown = () => {
    setDropdownShown(false);
  };

  const handleOutsideClick = event => {
    if (searchRef && !searchRef.current.contains(event.target)) {
      hideDropdown();
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", hideDropdown);
      window.addEventListener("mousedown", handleOutsideClick);

      return () => {
        window.removeEventListener("scroll", hideDropdown);
        window.removeEventListener("mousedown", handleOutsideClick);
      };
    }
  });

  useEffect(() => {
    if (dropdownShown) {
      document
        .querySelector(".navigation-logo")
        .classList.add("navigation-logo_search-expanded");
    } else {
      document
        .querySelector(".navigation-logo")
        .classList.remove("navigation-logo_search-expanded");
    }
  }, [dropdownShown]);

  return (
    <div
      className={classnames("navigation-search", {
        "navigation-search_expanded": dropdownShown
      })}
      ref={searchRef}
    >
      <div className="navigation-search__inner">
        <input
          type="text"
          className="navigation-search__input"
          placeholder="Поиск"
          value={query}
          onChange={handleChange}
          onFocus={() => setDropdownShown(query !== "")}
        />
        <div className="navigation-search__inner-icon">
          <SearchIcon />
        </div>
        <CSSTransition
          in={dropdownShown}
          timeout={300}
          unmountOnExit
          classNames={"navigation-search-dropdown-animation"}
        >
          <div className="navigation-search-dropdown-container">
            <CustomScroll>
              <div className="navigation-search-dropdown-container__inner">
                {data.map((section, sectionIndex) => (
                  <div
                    key={sectionIndex}
                    className="navigation-search-dropdown-container__section"
                  >
                    <div className="navigation-search-dropdown__title">
                      {section.title}
                    </div>
                    {section.data.map((element, elIndex) => (
                      <a
                        className="navigation-search-dropdown__link"
                        key={"s" + sectionIndex + "_" + elIndex}
                        href={element.link}
                      >
                        {element.title}
                      </a>
                    ))}
                  </div>
                ))}
              </div>
            </CustomScroll>
          </div>
        </CSSTransition>
      </div>
    </div>
  );
};

export default NavigationSearch;
