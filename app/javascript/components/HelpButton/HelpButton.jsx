import React, { useState, useRef, useEffect } from "react";
import Tooltip from "../Tooltip/Tooltip";
import { useOutsideClick } from "../../hooks/useOutsideClick";
import { useScrollEventListener } from "../../hooks/useScrollEventListener";
import classnames from "class-names";
import { useResizeEventListener } from "../../hooks/useResizeEventListener";

const HelpButton = () => {
  const [visible, setVisible] = useState(false);
  const [popupMode, setPopupMode] = useState("right");

  const ref = useRef(null);
  useOutsideClick(ref, () => setVisible(false));
  useScrollEventListener(() => setVisible(false));
  useResizeEventListener(() =>
    setPopupMode(window.innerWidth < 1024 ? "bottom" : "right")
  );

  useEffect(() => {
    window &&
      window.addEventListener("load", () => {
        setPopupMode(window.innerWidth < 1024 ? "bottom" : "right");
      });
  });

  return (
    <div ref={ref} className="page-jumbotron__help-container">
      <span
        className={classnames("page-jumbotron__help", {
          "page-jumbotron__help_active": visible
        })}
        onClick={() => setVisible(!visible)}
      >
        ?
      </span>
      <Tooltip
        className="appointment-form-tooltip"
        visible={visible}
        placement={popupMode}
      >
        <div className="appointment-form-tooltip__header">
          Через сайт вы можете записаться только на первичный или&nbsp;повторный
          прием.
        </div>
        <div className="appointment-form-tooltip__paragraph">
          Если вы уже были у того же врача вы течение последнего месяца, то вы
          автоматически запишитест на повторный прием.
        </div>
        <div className="appointment-form-tooltip__paragraph">
          Если вы хотите записаться на операцию или процедуру, пожалуйста,
          позвоните нам по номеру <nobr>+7 (4822) 65-65-10</nobr>
        </div>
      </Tooltip>
    </div>
  );
};

export default HelpButton;
