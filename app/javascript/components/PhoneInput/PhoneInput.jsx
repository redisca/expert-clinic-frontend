import React from "react";
import InputMask from "react-input-mask";
import classnames from "class-names";

const PhoneInput = ({ className }) => (
  <InputMask
    mask="+7 (999) 999 99 99"
    className={classnames("input-group__input", className)}
  />
);

export default PhoneInput;
