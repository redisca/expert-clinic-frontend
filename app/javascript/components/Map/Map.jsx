import React from "react";
import { YMaps, Map, Placemark } from "react-yandex-maps";

import PinSVG from "./../../../assets/images/icons/map-pin.svg";

const mapState = {
  center: [55.751574, 37.573856],
  zoom: 15,
  behaviors: ["drag", "dblClickZoom", "rightMouseButtonMagnifier", "multiTouch"]
};

export default () => (
  <div className="homepage-map">
    <YMaps>
      <Map defaultState={mapState} width="100%" height="100%">
        <Placemark
          geometry={[55.751574, 37.573856]}
          options={{
            iconLayout: "default#image",
            iconImageHref: PinSVG,
            iconImageSize: [35, 48],
            iconImageOffset: [-20, -24],
            openBalloonOnClick: false,
            openHintOnHover: false
          }}
        />
      </Map>
    </YMaps>
  </div>
);
