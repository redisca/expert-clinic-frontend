import React from "react";
import classnames from "class-names";
import { RedirectMessage } from "./DoctorSlots";

class DoctorCard extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = { expanded: false };
  }

  render() {
    const {
      firstName,
      lastName,
      description,
      photo,
      photoSrcSet,
      redirect,
      appointmentLink,
      size
    } = this.props;
    const { expanded } = this.state;

    return (
      <div
        className={classnames("doctor", {
          doctor_expanded: expanded,
          [`doctor_size_${size}`]: true
        })}
      >
        <div className="doctor-data">
          <div
            className="doctor-photo"
            style={{ backgroundImage: "url(" + photo + ")" }}
          >
            <img
              className="doctor-photo__image"
              alt={lastName + " " + firstName}
              src={photo}
              srcSet={photoSrcSet}
            />
          </div>
          <div className="doctor-info">
            <h3
              className={classnames("doctor-info__name", {
                [`doctor-info__name_size_${size}`]: true
              })}
            >
              <a href="/doctors/view">
                <span>{lastName}</span>
                <span>{firstName}</span>
              </a>
            </h3>
            <p
              className={classnames("doctor-info__description", {
                [`doctor-info__description_size_${size}`]: true
              })}
            >
              {description}
            </p>
          </div>
        </div>
        <div className="doctor__footer">
          {appointmentLink && (
            <a
              href={appointmentLink}
              className="button button_blue-border doctor__button"
            >
              Записаться
            </a>
          )}
          {redirect && <RedirectMessage redirect={redirect} />}
        </div>
      </div>
    );
  }
}

export default DoctorCard;
