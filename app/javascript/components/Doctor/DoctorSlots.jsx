import React from "react";
import classnames from "class-names";
import ArrowSVG from "./../icons/LongArrowRight";
import ControlArrowSvg from "./../icons/ArrowRight";
import ArrowUpSVG from "./../icons/ArrowUp";

const Controls = props => (
  <div className="doctor-slots-controls">
    <div className="doctor-slots-controls__control doctor-slots-controls__control_previous">
      <ControlArrowSvg />
    </div>
    <div className="doctor-slots-controls__header">{props.header}</div>
    <div className="doctor-slots-controls__control doctor-slots-controls__control_next">
      <ControlArrowSvg />
    </div>
  </div>
);

const Message = props => (
  <div
    className="doctor-slots-items__next-label"
    dangerouslySetInnerHTML={{ __html: props.message }}
  />
);

export const RedirectMessage = props => (
  <>
    <div className="doctor-slots-items__next-label">
      Ближайшая заявка на прием:
    </div>
    <a
      href="#"
      className="arrowed-link arrowed-link_blue doctor-slots__arrowed-link"
    >
      <span className="arrowed-link__text">{props.redirect}</span>
      <span className="arrowed-link__arrow">
        <ArrowSVG />
      </span>
    </a>
  </>
);

class DoctorSlots extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      expandable: false,
      expanded: false,
      expandedMaxHeight: null
    };

    this.expandAction = this.expandAction.bind(this);
    this.setIsExpandable = this.setIsExpandable.bind(this);
    this.calculateExpandedHeight = this.calculateExpandedHeight.bind(this);
    this.resizeHandler = this.resizeHandler.bind(this);

    this.slotsRef = React.createRef();
  }

  componentDidMount() {
    if (this.slotsRef) {
      setTimeout(this.setIsExpandable, 200);
    }

    if (typeof window !== "undefined") {
      window.addEventListener("resize", this.resizeHandler);
    }
  }

  componentWillUnmount() {
    if (typeof window !== "undefined") {
      window.removeEventListener("resize", this.resizeHandler);
    }
  }

  resizeHandler() {
    const { expanded } = this.state;
    if (expanded) {
      this.setState({ expandedMaxHeight: this.calculateExpandedHeight() });
    }
  }

  setIsExpandable() {
    const { clientHeight, scrollHeight } = this.slotsRef.current;

    if (clientHeight < scrollHeight) {
      this.setState({ expandable: true });
    }
  }

  expandAction() {
    const { onExpand } = this.props;

    const { expanded } = this.state;
    this.setState(
      {
        expanded: !expanded,
        expandedMaxHeight: expanded ? null : this.calculateExpandedHeight()
      },
      onExpand
    );
  }

  calculateExpandedHeight() {
    const { scrollHeight } = this.slotsRef.current;
    const padding = parseInt(
      getComputedStyle(this.slotsRef.current).paddingBottom,
      10
    );

    return scrollHeight + padding;
  }

  render() {
    const { slots, mobileHeader, redirect, message } = this.props;
    const { expandable, expanded, expandedMaxHeight } = this.state;

    return (
      <div
        className={classnames("doctor-slots doctors-list__doctor-slots", {
          "doctor-slots_text-only": !slots || redirect
        })}
      >
        {slots && <Controls header={mobileHeader} />}

        <div
          className={classnames("doctor-slots-items", {
            "doctor-slots-items_expanded": expanded,
            "doctor-slots-items_expandable": expandable
          })}
          style={{ maxHeight: expandedMaxHeight }}
          ref={this.slotsRef}
        >
          {redirect && <RedirectMessage redirect={redirect} />}
          {!slots && <Message message={message} />}
          {slots &&
            slots.map(day => (
              <div className="doctor-slots-column" key={day.date}>
                <div className="doctor-slots-column__date">{day.date}</div>
                <div className="doctor-slots-column__day">{day.day}</div>
                {day.items.map((slot, index) => (
                  <div
                    key={day.date + "_" + index}
                    className={classnames("doctor-slots-column__slot", {
                      "doctor-slots-column__slot_empty": slot.empty
                    })}
                  >
                    {slot.time}
                  </div>
                ))}
              </div>
            ))}
        </div>
      </div>
    );
  }
}

export default DoctorSlots;
