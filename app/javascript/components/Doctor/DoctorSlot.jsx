import React, { useState, useRef } from "react";
import classnames from "class-names";
import Tooltip from "../Tooltip/Tooltip";
import { useOutsideClick } from "../../hooks/useOutsideClick";
import { useScrollEventListener } from "../../hooks/useScrollEventListener";

const DoctorSlot = ({ className, disabledClassname, time, disabled }) => {
  const [visible, setVisible] = useState(false);
  const ref = useRef(null);
  useOutsideClick(ref, () => setVisible(false));
  useScrollEventListener(() => setVisible(false));

  return (
    <div style={{ position: "relative" }} ref={ref}>
      <div
        className={classnames("doctor-slots-column__slot", className, {
          [disabledClassname]: disabled,
          "doctor-slots-column__slot_no-hover": disabled
        })}
        onClick={() => (disabled ? setVisible(!visible) : setVisible(false))}
      >
        {time}
      </div>
      <Tooltip
        className="date-selector__tooltip"
        visible={visible}
        placement="bottom"
      >
        Это время уже занято. Возможно, оно свободно в других днях
      </Tooltip>
    </div>
  );
};

export default DoctorSlot;
