import React from "react";
import DayPicker from "react-day-picker";
import { format } from "date-fns";
import { ru } from "date-fns/locale";

const MONTHS = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Денкабрь"
];
const WEEKDAYS_LONG = [
  "Воскресенье",
  "Понедельник",
  "Вторник",
  "Среда",
  "Четверг",
  "Пятница",
  "Суббота"
];
const WEEKDAYS_SHORT = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"];

const NavbarElement = ({ month, onPreviousClick, onNextClick }) => (
  <div className="DayPicker-NavBar">
    <span
      className="DayPicker-NavButton DayPicker-NavButton--prev"
      onClick={() => onPreviousClick()}
    />
    <div className="DayPicker-Caption" role="heading">
      <span className="year">{format(month, "LLLL yyyy", { locale: ru })}</span>
    </div>
    <span
      onClick={() => onNextClick()}
      className="DayPicker-NavButton DayPicker-NavButton--next"
    />
  </div>
);

export default props => (
  <div className="expert-day-picker">
    <DayPicker
      months={MONTHS}
      navbarElement={<NavbarElement />}
      captionElement={() => null}
      weekdaysLong={WEEKDAYS_LONG}
      weekdaysShort={WEEKDAYS_SHORT}
      firstDayOfWeek={1}
      locale="ru"
      {...props}
    />
  </div>
);
