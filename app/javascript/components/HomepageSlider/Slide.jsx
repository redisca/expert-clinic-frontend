import React from "react";
import classnames from "class-names";
import ArrowSVG from "./../icons/LongArrowRight";

const Slide = props => (
  <a
    className={classnames("news-card", props.className, {
      "news-card_shadow": props.shadow,
      "news-card_central": props.central
    })}
    href={props.link}
  >
    <img src={props.image} alt={props.title} className="news-card__image" />

    <div className="news-card__content">
      {props.duration && (
        <div className="news-card__duration">{props.duration}</div>
      )}
      {props.tag && <div className="news-card__tag">{props.tag}</div>}

      <h3
        className="news-card__header"
        dangerouslySetInnerHTML={{ __html: props.title }}
      />
      {props.description && (
        <p className="news-card__description">{props.description}</p>
      )}
      {(props.newPrice || props.oldPrice) && (
        <div className="news-card__prices">
          {props.newPrice && (
            <span className="news-card__price-new">
              {props.newPrice} &#8381;
            </span>
          )}
          {props.oldPrice && (
            <span className="news-card__price-old">
              {props.oldPrice} &#8381;
            </span>
          )}
        </div>
      )}
      <div className="news-card__footer">
        <div className="arrowed-link arrowed-link_gray news-card__arrowed-link">
          <span className="arrowed-link__text">Подробнее</span>
          <span className="arrowed-link__arrow">
            <ArrowSVG />
          </span>
        </div>
      </div>
    </div>
  </a>
);

export default Slide;
