import React from "react";
import classnames from "class-names";
import Slide from "./Slide";
import ControlSVG from "./../icons/ArrowRight";

class HomepageSlider extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      offset: 0,
      noAnimate: false,
      inputBlocked: false,
      centralIndex: Math.floor((props.slides.length * 3) / 2)
    };

    this.innerRef = React.createRef();
    this.slideSize = 390;

    this.handleTransition = this.handleTransition.bind(this);
    this.scrollSlide = this.scrollSlide.bind(this);
    this.computeSlideSize = this.computeSlideSize.bind(this);
    this.isActive = this.isActive.bind(this);
  }

  componentDidMount() {
    if (this.innerRef.current) {
      this.innerRef.current.addEventListener(
        "transitionend",
        this.handleTransition
      );
      this.innerRef.current.addEventListener(
        "webkitTransitionEnd",
        this.handleTransition
      );

      if (typeof window !== "undefined") {
        window.addEventListener("resize", this.computeSlideSize);
      }

      // Dirty hack: Use timeout as offsetWidth returns 0 during componentDidMount
      setTimeout(this.computeSlideSize, 200);
    }
  }

  componentWillUnmount() {
    if (this.innerRef.current) {
      this.innerRef.current.removeEventListener(
        "transitionend",
        this.handleTransition
      );
      this.innerRef.current.removeEventListener(
        "webkitTransitionEnd",
        this.handleTransition
      );

      if (typeof window !== "undefined") {
        window.removeEventListener("resize", this.computeSlideSize);
      }
    }
  }

  computeSlideSize() {
    const firstSlide = this.innerRef.current.children[0];
    const oldValue = this.slideSize;

    this.slideSize =
      parseInt(firstSlide.offsetWidth, 10) +
      parseInt(getComputedStyle(firstSlide).marginLeft, 10) +
      parseInt(getComputedStyle(firstSlide).marginRight, 10);

    if (oldValue !== this.slideSize) {
      this.setState({ offset: 0 });
    }
  }

  handleTransition() {
    const { offset } = this.state;
    const { slides } = this.props;
    const { slideSize } = this;

    if (Math.abs(offset) >= slideSize * slides.length) {
      this.setState(
        {
          noAnimate: true,
          offset: offset % (slideSize * slides.length),
          centralIndex: Math.floor((slides.length * 3) / 2)
        },
        () => {
          setTimeout(
            () => this.setState({ noAnimate: false, inputBlocked: false }),
            100
          );
        }
      );
    } else {
      this.setState({ inputBlocked: false });
    }
  }

  scrollSlide(direction) {
    const { offset, inputBlocked, centralIndex } = this.state;
    const coeff = direction === "next" ? -1 : 1;

    return () =>
      !inputBlocked &&
      this.setState({
        offset: offset + coeff * this.slideSize,
        inputBlocked: true,
        centralIndex: centralIndex - coeff
      });
  }

  isActive(index) {
    const { centralIndex } = this.state;
    if (!window) {
      return false;
    }

    if (window && window.innerWidth > 1023) {
      return index >= centralIndex - 1 && index <= centralIndex + 1;
    } else {
      return index === centralIndex;
    }
  }

  render() {
    const { offset, noAnimate } = this.state;
    const { slides } = this.props;

    return (
      <section className="news">
        <div className="container">
          <div className="news-header">
            <h2 className="section-header">Новости и акции</h2>
            <div className="news-slider__controls">
              <div
                className="news-slider__arrow news-slider__arrow_left"
                onClick={this.scrollSlide("prev")}
              >
                <ControlSVG />
              </div>
              <div
                className="news-slider__arrow news-slider__arrow_right"
                onClick={this.scrollSlide("next")}
              >
                <ControlSVG />
              </div>
            </div>
            <a href="#" className="button button_white news-header__button">
              Смотреть все
            </a>
          </div>
        </div>

        <div className="news-slider">
          <div
            className={classnames("news-slider-inner", { noAnimate })}
            style={{ transform: "translateX(" + offset + "px)" }}
            ref={this.innerRef}
          >
            {slides.map((slide, index) => (
              <Slide
                key={"prev" + index}
                central={this.isActive(index)}
                {...slide}
                shadow
              />
            ))}
            {slides.map((slide, index) => (
              <Slide
                key={"curr" + index}
                central={this.isActive(slides.length + index)}
                {...slide}
              />
            ))}
            {slides.map((slide, index) => (
              <Slide
                key={"next" + index}
                central={this.isActive(slides.length * 2 + index)}
                {...slide}
                shadow
              />
            ))}
          </div>
        </div>

        <a href="#" className="button button_white news__button">
          Смотреть все
        </a>
      </section>
    );
  }
}

export default HomepageSlider;
