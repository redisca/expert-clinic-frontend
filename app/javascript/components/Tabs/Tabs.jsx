import React, { useEffect, useState } from "react";
import DumbTabs from "./DumbTabs";
import ScrollingTabs from "./ScrollingTabs";
import ExpandingTabs from "./ExpandingTabs";

const Tabs = ({ links }) => {
  const isMobile = () => window && window.innerWidth < 768;
  const isTablet = () =>
    window && window.innerWidth >= 768 && window.innerWidth < 1024;
  const isDesktop = () => window && window.innerWidth >= 1024;

  const getMode = () => {
    if (isDesktop()) {
      return "desktop";
    } else if (isTablet()) {
      return "tablet";
    } else if (isMobile()) {
      return "mobile";
    }
  };

  const [mode, setMode] = useState(getMode());

  const changeMode = () => {
    setMode(getMode());
  };

  useEffect(() => {
    if (window) {
      window.addEventListener("resize", changeMode);
      window.addEventListener("load", changeMode);

      return () => {
        window.removeEventListener("resize", changeMode);
        window.removeEventListener("load", changeMode);
      };
    }
  });

  if (mode === "desktop") {
    return <DumbTabs links={links} />;
  } else if (mode === "tablet") {
    return <ScrollingTabs links={links} />;
  } else if (mode === "mobile") {
    return <ExpandingTabs links={links} />;
  }
  return null;
};

export default Tabs;
