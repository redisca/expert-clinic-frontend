import React from "react";
import Tabs from "./Tabs";
import LogoutIcon from "./../icons/Logout";

export default ({ active }) => {
  return (
    <Tabs
      links={[
        {
          title: "Записи и результаты",
          href: "/cabinet/appointments",
          active: active === 0
        },
        { title: "Мои врачи", href: "/cabinet/doctors", active: active === 1 },
        {
          title: "Мой профиль",
          href: "/cabinet/profile",
          active: active === 2
        },
        {
          title: "Персональные предложения",
          href: "/cabinet/offers",
          active: active === 3
        },
        {
          title: (
            <>
              <LogoutIcon />
              Выйти
            </>
          ),
          href: "#",
          className: "cabinet-page__logout-link"
        }
      ]}
    />
  );
};
