import React from "react";
import classnames from "class-names";

const DumbTabs = ({ links, linkClickHander }) => (
  <div className="preload tabs">
    {links &&
      links.map(({ title, href, active, className }) => (
        <div
          key={href}
          className={classnames("tabs__tab", className, {
            tabs__tab_active: active
          })}
        >
          <a className="tabs__link" href={href} onClick={linkClickHander}>
            {title}
          </a>
        </div>
      ))}
  </div>
);

export default DumbTabs;
