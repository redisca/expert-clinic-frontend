import React from "react";
import classnames from "class-names";

const DumbTabs = ({ links, linkClickHander }) => (
  <div className="buttons-tabs">
    {links &&
      links.map(({ title, href, active, className }, index) => (
        <div
          key={href || index}
          className={classnames("buttons-tabs__tab", className, {
            "buttons-tabs__tab_active": active
          })}
        >
          <a
            className="buttons-tabs__link"
            href={href}
            onClick={linkClickHander}
          >
            {title}
          </a>
        </div>
      ))}
  </div>
);

export default DumbTabs;
