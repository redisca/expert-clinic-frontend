import React, { useRef, useState, useEffect } from "react";
import DumbTabs from "./DumbTabs";
import isTouchDevice from "is-touch-device";

const ScrollingTabs = ({ links }) => {
  const containerRef = useRef(null);
  const [{ moving, startScroll, startMouse }, setState] = useState({
    moving: false,
    startScroll: 0,
    startMouse: 0
  });
  const [lastMovementTime, setLastMovementTime] = useState(null);
  const [lastDelta, setLastDelta] = useState(null);

  const beginMoving = event => {
    setState({
      moving: true,
      startScroll: containerRef.current.scrollLeft,
      startMouse: event.clientX
    });
  };

  const endMoving = event => {
    setLastDelta(Math.abs(startMouse - event.clientX));
    setState({
      moving: false,
      startScroll: containerRef.current.scrollLeft,
      startMouse: 0
    });
  };

  const moveHandler = event => {
    if (!moving) {
      return;
    }

    containerRef.current.scrollLeft =
      startScroll + (startMouse - event.clientX);
    setLastMovementTime(new Date());
  };

  useEffect(() => {
    if (document && !isTouchDevice()) {
      document.addEventListener("mouseup", endMoving);
      document.addEventListener("mousemove", moveHandler);

      return () => {
        document.removeEventListener("mouseup", endMoving);
        document.removeEventListener("mousemove", moveHandler);
      };
    }
  });

  const linkClickHandler = event => {
    if (
      new Date() - lastMovementTime < 200 &&
      !(lastDelta !== null && lastDelta < 5)
    ) {
      event.preventDefault();
    }
  };

  return (
    <div
      className="tabs__scroll-container"
      ref={containerRef}
      onMouseDown={isTouchDevice() ? null : beginMoving}
      onMouseMove={isTouchDevice() ? null : moveHandler}
    >
      <DumbTabs links={links} linkClickHander={linkClickHandler} />
    </div>
  );
};

export default ScrollingTabs;
