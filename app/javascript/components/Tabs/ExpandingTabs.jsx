import React, { useState, useEffect } from "react";
import DumbTabs from "./DumbTabs";
import { CSSTransition } from "react-transition-group";
import classnames from "class-names";

const ExpandingTabs = ({ links }) => {
  const isMobile = () => window && window.innerWidth < 768;
  const [dropdownShown, setDropdownShown] = useState(false);

  const linkClickHander = event => {
    if (isMobile()) {
      setDropdownShown(!dropdownShown);
      event.preventDefault();
    }
  };

  const handleResize = () => {
    if (!isMobile()) {
      setDropdownShown(false);
    }
  };

  useEffect(() => {
    if (window) {
      window.addEventListener("resize", handleResize);

      return () => {
        window.removeEventListener("resize", handleResize);
      };
    }
  });

  return (
    <div className={classnames({ "tabs-dropdown_visible": dropdownShown })}>
      <DumbTabs links={links} linkClickHander={linkClickHander} />
      <CSSTransition
        in={dropdownShown}
        timeout={300}
        unmountOnExit
        classNames={"navigation-search-dropdown-animation"}
      >
        <div className="tabs-dropdown">
          {links.map(item => (
            <a
              href={item.href}
              key={item.href}
              className={classnames("tabs-dropdown__link", item.className)}
            >
              {item.title}
            </a>
          ))}
        </div>
      </CSSTransition>
    </div>
  );
};

export default ExpandingTabs;
