import React, { useEffect, useState } from "react";
import ButtonsTabs from "./ButtonsTabs";
import ExpandingTabs from "./ExpandingTabs";

const PromosTabs = ({ links }) => {
  const isMobile = () => window && window.innerWidth < 768;

  const getMode = () => {
    if (isMobile()) {
      return "mobile";
    }
    return "desktop";
  };

  const [mode, setMode] = useState(getMode());

  const changeMode = () => {
    setMode(getMode());
  };

  useEffect(() => {
    if (window) {
      window.addEventListener("resize", changeMode);

      return () => {
        window.removeEventListener("resize", changeMode);
      };
    }
  });

  if (mode === "mobile") {
    return <ExpandingTabs links={links} />;
  }
  return <ButtonsTabs links={links} />;
};

export default PromosTabs;
