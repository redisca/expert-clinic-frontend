import React, { useState, useRef } from "react";
import Tooltip from "./../Tooltip/Tooltip";
import PinIcon from "./../icons/Pin.jsx";
import ArrowDown from "./../icons/ArrowDown";
import { useScrollEventListener } from "../../hooks/useScrollEventListener";
import { useOutsideClick } from "../../hooks/useOutsideClick";

const AddressesTooltip = () => {
  const [visible, setVisible] = useState(false);
  const containerRef = useRef(null);
  useScrollEventListener(() => setVisible(false));
  useOutsideClick(containerRef, () => setVisible(false));

  return (
    <div ref={containerRef} className="address-tooltip__container">
      <a
        href="#"
        className="navigation-links__link navigation-links__link_addresses"
        onClick={() => setVisible(!visible)}
      >
        <PinIcon />
        <span>Адреса центров</span>
        <span className="navigation-links__arrow">
          <ArrowDown />
        </span>
      </a>
      <Tooltip visible={visible} className="address-tooltip">
        <div className="address-tooltip__header">
          Центр на проезде Швейников
        </div>
        <a href="#" className="address-tooltip__info">
          +7 (4822) 65-65-10
        </a>
        <div className="address-tooltip__info">пр. Швейников, д.1</div>
        <div className="address-tooltip__info">пн-вс: с 07:00 до 23:00</div>

        <a href="#" className="map-link address-tooltip__map-link">
          <PinIcon />
          Показать на карте
        </a>
      </Tooltip>
    </div>
  );
};

export default AddressesTooltip;
