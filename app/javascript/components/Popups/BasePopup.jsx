import React, { useState, useEffect, useRef } from "react";
import CloseIcon from "./../icons/Close";
import classnames from "class-names";
import { CSSTransition } from "react-transition-group";
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks
} from "body-scroll-lock";
import { useOutsideClick } from "../../hooks/useOutsideClick";

export const hidePopup = name => {
  const hideEvent = document.createEvent("Event");
  hideEvent.initEvent("hide", true, true);

  const popup = document.querySelector("#" + name);
  if (popup) {
    popup.dispatchEvent(hideEvent);
  }
};

const Backdrop = ({ onClick }) => (
  <div onClick={onClick} className="popup-fade" />
);

const Close = ({ onClick }) => (
  <div className="popup-close" onClick={onClick}>
    <CloseIcon />
  </div>
);

const BasePopup = ({ children, name, onHide, className }) => {
  const [visible, setVisible] = useState(false);
  const popupRef = useRef(null);
  const wrapperRef = useRef(null);
  const contentRef = useRef(null);

  const showPopup = () => {
    setVisible(true);
  };
  const hidePopup = () => {
    setVisible(false);
  };

  const onPopupHide = () => {
    onHide && onHide();
    enableBodyScroll(wrapperRef.current, {
      reserveScrollBarGap: true
    });
  };

  useEffect(() => {
    const element = popupRef.current;
    if (element) {
      element.addEventListener("show", showPopup, false);
      element.addEventListener("hide", hidePopup, false);

      return () => {
        element.removeEventListner("show", showPopup);
        element.removeEventListner("hide", hidePopup);
      };
    }
  }, []);

  useEffect(() => {
    if (visible) {
      disableBodyScroll(wrapperRef.current, {
        reserveScrollBarGap: true
      });
    }
  }, [visible]);

  useEffect(() => {
    return () => {
      clearAllBodyScrollLocks();
    };
  }, []);

  return (
    <div id={name} ref={popupRef} className="popup__wrapper">
      <CSSTransition
        in={visible}
        onExited={onPopupHide}
        timeout={300}
        unmountOnExit
        classNames={"popup-animation"}
      >
        <div className={"popup-container"}>
          <Backdrop onClick={hidePopup} />
          <div
            className="popup-wrapper"
            ref={wrapperRef}
            onClick={e => {
              if (e.target === wrapperRef.current && window.innerWidth >= 768) {
                hidePopup();
              }
            }}
          >
            <div className={classnames("popup", className)} ref={contentRef}>
              <Close onClick={hidePopup} />
              {children}
            </div>
          </div>
        </div>
      </CSSTransition>
    </div>
  );
};

export default BasePopup;
