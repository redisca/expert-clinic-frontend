import React from "react";
import BasePopup from "./BasePopup";

export const PopupContent = ({ header, children, footer }) => (
  <>
    {header && <div className="popup-header">{header}</div>}
    {children}
    {footer && <div className="popup-footer">{footer}</div>}
  </>
);

const Popup = ({ name, header, footer, children, className, onHide }) => {
  return (
    <BasePopup name={name} className={className} onHide={onHide}>
      <PopupContent header={header} footer={footer}>
        {children}
      </PopupContent>
    </BasePopup>
  );
};

export default Popup;
