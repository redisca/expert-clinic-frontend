import React from "react";
import Popup from "./Popup";

const JobStoryPopup = ({ name, personName, tag, text, photo, photoSrcSet }) => (
  <Popup name={name} className="popup-job-story">
    <div className="popup-job-story__content">
      <div className="popup-job-story__tag">{tag}</div>
      <div className="popup-job-story__name">{personName}</div>
    </div>
    <img src={photo} srcSet={photoSrcSet} className="popup-job-story__image" />
    <div className="popup-job-story__content">
      <div className="popup-job-story__text">{text}</div>
    </div>
  </Popup>
);

export default JobStoryPopup;
