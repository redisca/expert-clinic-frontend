import React, { useState } from "react";
import Popup from "./PopupWithMessage";
import PhoneInput from "./../PhoneInput/PhoneInput";

const SendButton = ({ onClick }) => (
  <a
    href="#"
    onClick={e => {
      onClick(e);
      e.preventDefault();
    }}
    className="button popup-footer__button"
  >
    Отправить
  </a>
);

const DoctorReviewPopup = ({ name }) => {
  const [messageShown, setMessageShown] = useState(false);
  const hideMessage = () => {
    setMessageShown(false);
  };

  return (
    <Popup
      name={name}
      onHide={hideMessage}
      header="Оставьте свой отзыв о враче"
      footer={
        <SendButton
          onClick={() => {
            setMessageShown(true);
          }}
        />
      }
      message="Спасибо за отзыв!"
      messageShown={messageShown}
      hideMessage={hideMessage}
    >
      <div className="popup-form">
        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Ваше имя</label>
          <input className="input-group__input" />
        </div>

        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Электронная почта</label>
          <input className="input-group__input" type="email" />
        </div>

        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Телефон</label>
          <PhoneInput />
        </div>

        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Отзыв</label>
          <textarea className="input-group__textarea" />
        </div>

        <div className="input-group">
          <label className="input-group__label input-group__label_checkbox">
            <input type="checkbox" className="input-group__checkbox" />
            <div className="input-group__checkbox-state" />
            Выражаю согласие на обработку персональных данных в соответствии
            с условиями.
          </label>
        </div>
      </div>
    </Popup>
  );
};

export default DoctorReviewPopup;
