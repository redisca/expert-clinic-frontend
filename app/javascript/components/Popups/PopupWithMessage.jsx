import React from "react";
import BasePopup from "./BasePopup";
import { PopupContent } from "./Popup";

const PopupWithMessage = ({
  name,
  header,
  footer,
  message,
  children,
  messageShown,
  onHide
}) => {
  return (
    <BasePopup name={name} onHide={onHide}>
      {messageShown ? (
        <div className="popup-message">{message}</div>
      ) : (
        <PopupContent header={header} footer={footer}>
          {children}
        </PopupContent>
      )}
    </BasePopup>
  );
};

export default PopupWithMessage;
