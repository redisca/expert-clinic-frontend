import React, { useState } from "react";
import Popup from "./PopupWithMessage";

const SendButton = ({ onClick }) => (
  <a
    href="#"
    onClick={e => {
      onClick(e);
      e.preventDefault();
    }}
    className="button popup-footer__button"
  >
    Отправить
  </a>
);

const DoctorQuestionPopup = ({ name }) => {
  const [messageShown, setMessageShown] = useState(false);
  const hideMessage = () => {
    setMessageShown(false);
  };

  return (
    <Popup
      name={name}
      onHide={hideMessage}
      header="Задать вопрос врачу"
      footer={
        <SendButton
          onClick={() => {
            setMessageShown(true);
          }}
        />
      }
      message="Спасибо за вопрос!"
      messageShown={messageShown}
      hideMessage={hideMessage}
    >
      <div className="popup-description popup-description_indent-b_large">
        Доктор ответит вам в ближайшее время. Обращаем внимание, что&nbsp;этот
        ответ не является консультацией.
      </div>

      <div className="popup-form">
        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Ваше имя</label>
          <input className="input-group__input" />
        </div>

        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Электронная почта</label>
          <input className="input-group__input" type="email" />
        </div>
        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Вопрос</label>
          <textarea className="input-group__textarea" />
        </div>
        <div className="input-group">
          <label className="input-group__label input-group__label_checkbox">
            <input type="checkbox" className="input-group__checkbox" />
            <div className="input-group__checkbox-state" />
            Выражаю согласие на обработку персональных данных в соответствии
            с условиями.
          </label>
        </div>
      </div>
    </Popup>
  );
};

export default DoctorQuestionPopup;
