import React, { useState } from "react";
import Popup from "./PopupWithMessage";
import PhoneInput from "./../PhoneInput/PhoneInput";
import SelectInput from "../SelectInput/SelectInput";

const SendButton = ({ onClick }) => (
  <a
    href="#"
    onClick={e => {
      onClick(e);
      e.preventDefault();
    }}
    className="button popup-footer__button"
  >
    Отправить
  </a>
);

const doctors = {
  1: "Шелест Петр Викторович",
  2: "Наседкина Галина Григорьевна",
  3: "Пермяков Сергей Владимирович",
  4: "Ахлакова Патимат Магомедовна",
  5: "Анненков Андрей Викторович",
  11: "Шелест Петр Викторович",
  22: "Наседкина Галина Григорьевна",
  33: "Пермяков Сергей Владимирович",
  44: "Ахлакова Патимат Магомедовна",
  55: "Анненков Андрей Викторович"
};

const specializations = {
  1: "Ведение беременности",
  2: "Гинекология",
  3: "Гирудотерапия",
  4: "Генетика",
  5: "Кардиология",
  6: "Хирургия"
};

const DoctorAbstractReviewPopup = ({ name }) => {
  const [messageShown, setMessageShown] = useState(false);
  const hideMessage = () => {
    setMessageShown(false);
  };

  return (
    <Popup
      name={name}
      onHide={hideMessage}
      header="Оставить отзыв"
      footer={
        <SendButton
          onClick={() => {
            setMessageShown(true);
          }}
        />
      }
      message="Спасибо за отзыв!"
      messageShown={messageShown}
      hideMessage={hideMessage}
    >
      <div className="popup-form">
        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Ваше имя</label>
          <input className="input-group__input" />
        </div>

        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Электронная почта</label>
          <input className="input-group__input" type="email" />
          <div className="input-group__hint">
            Личные данные не будут отражены при публикации отзыва на сайте.
          </div>
        </div>

        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Специалист</label>
          <SelectInput options={doctors} />
        </div>

        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Направление</label>
          <SelectInput options={specializations} />
        </div>

        <div className="input-group input-group_indent-b_default">
          <label className="input-group__label">Отзыв</label>
          <textarea className="input-group__textarea" />
        </div>

        <div className="input-group">
          <label className="input-group__label input-group__label_checkbox">
            <input type="checkbox" className="input-group__checkbox" />
            <div className="input-group__checkbox-state" />
            Выражаю согласие на обработку персональных данных в соответствии
            с условиями.
          </label>
        </div>
      </div>
    </Popup>
  );
};

export default DoctorAbstractReviewPopup;
