import React from "react";
import Popup from "./Popup";
import classnames from "class-names";

const citiesList = [
  {
    letter: "А",
    list: [
      "Абакан",
      "Азов",
      "Алексин",
      "Альметьевск",
      "Анапа",
      "Анжеро-Судженск",
      "Ачинск"
    ]
  },
  {
    letter: "Б",
    list: ["Боровичи", "Братск", "Брянск", "Буйнакск"]
  },
  {
    letter: "В",
    list: ["Воскресенск", "Выборг", "Выкса", "Вязьма"]
  },
  {
    letter: "Г",
    list: ["Грозный", "Губкин", "Гудермес", "Гуково"]
  },
  {
    letter: "Д",
    list: ["Долгопрудный", "Домодедово", "Донской", "Дубна"]
  },
  {
    letter: "E",
    list: ["Евпатория", "Екатеринбург", "Елабуга", "Елец", "Ессентуки"]
  },
  {
    letter: "Ж",
    list: ["Железногорск", "Жигулевск", "Жуковский"]
  },
  {
    letter: "З",
    list: ["Заречный", "Зеленогорск", "Зеленодольск", "Златоуст"]
  },
  {
    letter: "И",
    list: ["Иркутск", "Искитим", "Ишим", "Ишимбай"]
  },
  {
    letter: "Й",
    list: ["Йошкар-Ола"]
  },
  {
    letter: "К",
    list: ["Котлас", "Красногорск", "Краснодар", "Кузнецк", "Кумертау", "Кызыл"]
  },
  {
    letter: "Л",
    list: ["Лиски", "Лобня", "Лысьва", "Лыткарино", "Люберцы"]
  },
  {
    letter: "М",
    list: ["Магадан", "Магнитогорск", "Мичуринск"],
    className: "popup-city-select__cities-group_indent-b_none"
  },
  { letter: "M", list: ["Москва", "Мурманск", "Муром", "Мытищи"] },
  {
    letter: "Н",
    list: ["Новочеркасск", "Новошахтинск", "Новый Уренгой", "Ногинск"]
  },
  {
    letter: "О",
    list: [
      "Обнинск",
      "Одинцово",
      "Омск",
      "Орел",
      "Оренбург",
      "Орехово-Зуево",
      "Орск"
    ]
  },
  {
    letter: "П",
    list: ["Павлово", "Павл. Пассад", "Пермь", "Псков", "Пушкино", "Пятигорск"]
  },
  {
    letter: "Р",
    list: ["Раменское", "Ревда", "Рубцовск", "Рыбинск", "Рязань"]
  },
  {
    letter: "С",
    list: ["Самара", "Санкт-Петербург"]
  },
  { letter: "C", list: ["Саранск", "Сарапул", "Сызрань", "Сыктывкар"] },
  {
    letter: "Т",
    list: ["Тобольск", "Тольятти", "Томск", "Тула", "Тюмень"]
  },
  {
    letter: "У",
    list: ["Узловая", "Улан-Удэ", "Ульяновск", "Урус-Мартан"]
  },
  {
    letter: "Ч",
    list: ["Черногорск", "Чехов", "Чистополь", "Чита"]
  },
  {
    letter: "Щ",
    list: ["Щекино", "Щелково"]
  },
  {
    letter: "Ю",
    list: ["Южно-Сахалинск", "Юрга"]
  },
  {
    letter: "Я",
    list: ["Якутск", "Ялта", "Ярославль"]
  }
];

export const CitySelectPopupContent = () => (
  <div>
    <div className="popup-city-select__search">
      <input placeholder="Введите город" />
    </div>

    <div className="popup-city-select__cities">
      {citiesList.map(el => {
        return (
          <div
            className={classnames(
              "popup-city-select__cities-group",
              el.className
            )}
            key={el.letter}
          >
            <div
              key={el.letter}
              className="popup-city-select__cities-group-header"
            >
              {el.letter}
            </div>
            {el.list.map(city => (
              <a key={city} href="#" className="popup-city-select__city">
                {city}
              </a>
            ))}
          </div>
        );
      })}
    </div>
  </div>
);

const CitySelectPopup = ({ name }) => {
  return (
    <Popup className="popup-city-select" name={name} header="Выберите город">
      <CitySelectPopupContent />
    </Popup>
  );
};

export default CitySelectPopup;
