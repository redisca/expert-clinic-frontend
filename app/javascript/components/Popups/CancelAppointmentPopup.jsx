import React, { useState } from "react";
import Popup from "./Popup";
import { hidePopup } from "./BasePopup";
import classnames from "class-names";

const CancelConfirmationContent = ({ onNo, onYes }) => (
  <div className="popup-cancel-appointment__actions-block popup-cancel-appointment__actions-block_width_small">
    <a
      href="#"
      onClick={e => {
        onYes && onYes();
        e.preventDefault();
      }}
      className="button button_large"
    >
      Отменить заявку
    </a>
    <a
      href="#"
      onClick={e => {
        onNo && onNo();
        e.preventDefault();
      }}
      className="button button_blue-border button_large"
    >
      Продолжить
    </a>
  </div>
);

const CanceledContent = () => (
  <div>
    <div className="popup-cancel-appointment__message">
      Если вы ошиблись или отписались случайно, то&nbsp;нажмите «Оставить новую
      заявку»
    </div>
    <div className="popup-cancel-appointment__actions-block popup-cancel-appointment__actions-block_width_large">
      <a
        href="#"
        onClick={e => {
          e.preventDefault();
        }}
        className="button button_large"
      >
        Изменить время
      </a>
      <a
        href="#"
        onClick={e => {
          e.preventDefault();
        }}
        className="button button_blue-border button_large"
      >
        Оставить новую заявку
      </a>
    </div>
  </div>
);

const CancelAppointmentPopup = ({ name }) => {
  const [canceled, setCanceled] = useState(false);

  return (
    <Popup
      name={name}
      className={classnames("popup-cancel-appointment", {
        "popup-cancel-appointment_space-b_small": canceled
      })}
      onHide={() => setCanceled(false)}
      header={
        canceled
          ? "Ваша заявка отменена"
          : "Вы действительно хотите отменить заявку?"
      }
    >
      {canceled ? (
        <CanceledContent />
      ) : (
        <CancelConfirmationContent
          onYes={() => setCanceled(true)}
          onNo={() => {
            hidePopup(name);
          }}
        />
      )}
    </Popup>
  );
};
export default CancelAppointmentPopup;
