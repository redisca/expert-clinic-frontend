import React, { useState } from "react";
import Popup from "./Popup";
import { CitySelectPopupContent } from "./CitySelectPopup";

const ConfirmationPopupContent = ({ onYes, onNo }) => (
  <div>
    <a
      href="#"
      onClick={e => {
        onYes && onYes();
        e.preventDefault();
      }}
      className="button button_large popup-city-confirmation__button"
    >
      Да, верно
    </a>
    <a
      href="#"
      onClick={e => {
        onNo && onNo();
        e.preventDefault();
      }}
      className="button button_large button_blue-border popup-city-confirmation__button"
    >
      Другой город
    </a>
  </div>
);

const ConfirmCityPopup = ({ name }) => {
  const [showCitySelect, setShowCitySelect] = useState(false);

  return (
    <Popup
      className={
        showCitySelect ? "popup-city-select" : "popup-city-confirmation"
      }
      name={name}
      onHide={() => setShowCitySelect(false)}
      header={showCitySelect ? "Выберите город" : "Ваш город — Иркутск?"}
    >
      {showCitySelect ? (
        <CitySelectPopupContent />
      ) : (
        <ConfirmationPopupContent onNo={() => setShowCitySelect(true)} />
      )}
    </Popup>
  );
};
export default ConfirmCityPopup;
