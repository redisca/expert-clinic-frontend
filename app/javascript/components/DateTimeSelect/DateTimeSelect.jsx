import React from "react";
import classnames from "class-names";
import CalendarFilter from "./../Filter/CalendarFilter";
import { format } from "date-fns";
import { ru } from "date-fns/locale";
import DropdownFilter from "../Filter/DropdownFilter";

const time = {
  0: "Любое",
  1: "с 8:00 до 11:00",
  2: "с 11:00 до 14:00",
  3: "с 14:00 до 17:00",
  4: "с 17:00 до 20:00"
};

const DateTimeSelect = ({ className }) => (
  <div className={classnames("date-selector", className)}>
    <div className="date-selector__calendar-wrapper">
      <CalendarFilter
        title="Выберите дату"
        className="appointment-form__filter"
        defaultValue={new Date()}
        getTitle={day => (
          <>
            <span className="day-of-week">
              {format(day, "EEEE", { locale: ru })},&nbsp;
            </span>
            {format(day, "d MMMM", { locale: ru })}
          </>
        )}
      />
    </div>
    <div className="date-selector__slots-wrapper date-selector__slots-wrapper_space-v_none date-selector__slots-wrapper_space-h_none">
      <DropdownFilter
        name="time"
        className="appointment-form__filter"
        getTitle={value => time[value]}
        title="Выберите желаемое время"
        values={time}
        defaultValue={0}
      />
    </div>
  </div>
);

export default DateTimeSelect;
