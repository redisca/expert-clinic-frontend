import React from "react";
import Filter from "./Filter";
import DayPicker from "./../DayPicker/DayPicker";

const CalendarDayPicker = ({ setValue, value, ...props }) => (
  <DayPicker
    {...props}
    selectedDays={value}
    onDayClick={day => setValue(day)}
  />
);

const DropdownFilter = ({ name, getTitle, title, defaultValue, className }) => {
  console.log("ren");
  return (
    <Filter {...{ name, title, defaultValue, getTitle, className }}>
      <CalendarDayPicker />
    </Filter>
  );
};

export default DropdownFilter;
