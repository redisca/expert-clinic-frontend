import React from "react";
import Filter from "./Filter";
import Dropdown from "./Dropdown";

const DropdownFilter = ({
  name,
  getTitle,
  search,
  title,
  searchPlaceholder,
  defaultValue,
  values,
  className
}) => {
  return (
    <Filter {...{ name, title, defaultValue, getTitle, className }}>
      <Dropdown {...{ search, searchPlaceholder, values }} />
    </Filter>
  );
};

export default DropdownFilter;
