import React, { useState } from "react";
import CustomScroll from "react-custom-scroll";

const DropdownFilter = ({ search, searchPlaceholder, values, setValue }) => {
  const [searchQuery, setSearchQuery] = useState("");

  const filteredValues = search
    ? Object.keys(values).filter(key =>
        values[key].toLowerCase().includes(searchQuery.toLowerCase())
      )
    : Object.keys(values);

  return (
    <CustomScroll>
      <div className="filter-dropdown-inner">
        {search && (
          <input
            className="filter-dropdown__input"
            type="text"
            value={searchQuery}
            onChange={e => setSearchQuery(e.target.value)}
            placeholder={searchPlaceholder}
          />
        )}
        {filteredValues.map(key => (
          <div
            key={key}
            className="filter-dropdown__item"
            onClick={() => setValue(key)}
          >
            {values[key]}
          </div>
        ))}
        {search && filteredValues.length === 0 && (
          <div className="filter-dropdown__item">Ничего не найдено</div>
        )}
      </div>
    </CustomScroll>
  );
};

export default DropdownFilter;
