import React, { useState, useEffect, useRef } from "react";
import classnames from "class-names";
import { CSSTransition } from "react-transition-group";
import ArrowSVG from "../icons/ArrowUp";

const Filter = ({ title, children, getTitle, defaultValue, className }) => {
  const [dropdownShown, setDropdownShown] = useState(false);
  const [value, setValue] = useState(defaultValue);
  const containerRef = useRef(null);

  const toggleDropdown = () => setDropdownShown(!dropdownShown);
  const handleOutsideClick = event => {
    if (containerRef && !containerRef.current.contains(event.target)) {
      setDropdownShown(false);
    }
  };
  const handlePageScroll = event => {
    if (
      window.innerWidth >= 768 &&
      !containerRef.current.contains(document.activeElement)
    ) {
      // This check prevents hiding dropdown when keyboard appears on mobile devices
      setDropdownShown(false);
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", handlePageScroll);
      window.addEventListener("mousedown", handleOutsideClick);
    }

    return () => {
      window.removeEventListener("scroll", handlePageScroll);
      window.removeEventListener("mousedown", handleOutsideClick);
    };
  });

  const enhancedChildren = React.Children.map(children, child => {
    return React.cloneElement(child, {
      value,
      setValue: value => {
        setValue(value);
        setDropdownShown(false);
      }
    });
  });

  return (
    <div
      ref={containerRef}
      className={classnames("filter", className, {
        filter_expanded: dropdownShown
      })}
    >
      <div className="filter-inner" onClick={toggleDropdown}>
        <div className="filter__title">{title}</div>
        <div className="filter__value">
          {getTitle ? getTitle(value) : value}
        </div>
        <div className="filter-controls">
          <div className="filter-controls__control">
            <ArrowSVG />
          </div>
        </div>
      </div>
      <CSSTransition
        in={dropdownShown}
        timeout={300}
        unmountOnExit
        classNames={"filter-dropdown-animation"}
      >
        <div className="filter-dropdown">{enhancedChildren}</div>
      </CSSTransition>
    </div>
  );
};

export default Filter;
