import React from "react";

export default () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 26">
    <g fill="none" fillRule="evenodd" stroke="#4A4A4A">
      <path d="M23.34 11.472H11.49m8.536-3.316l3.315 3.316-3.316 3.315M7.583 19.534h7.812v-5.052m0-6.02V1.475H.482" />
      <path d="M1 2v18.305l7.101 4.307V6.307z" />
    </g>
  </svg>
);
