import React from "react";

const SearchIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
    <path
      fill="none"
      fillRule="evenodd"
      stroke="#4A4A4A"
      strokeWidth="2"
      d="M9 17A8 8 0 1 0 9 1a8 8 0 0 0 0 16zm5.5-1.5l5.803 6.803"
    />
  </svg>
);

export default SearchIcon;
