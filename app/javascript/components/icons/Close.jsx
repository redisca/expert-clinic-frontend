import React from "react";

export default () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23 22">
    <g fill="none" fillRule="evenodd">
      <path d="M-1-1h24v24H-1z" />
      <path
        d="M1.5 1.5l19.506 19.506M21.5 1L1.994 20.506"
        stroke="#A4A4A4"
        strokeWidth="2"
      />
    </g>
  </svg>
);
