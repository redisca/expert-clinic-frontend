import React from "react";

export default () => (
  <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M9 20l7-7.296L9 5"
      stroke="#A4A4A4"
      strokeWidth="2"
      fill="none"
      fillRule="evenodd"
    />
  </svg>
);
