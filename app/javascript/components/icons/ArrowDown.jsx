import React from "react";

export default () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
    <path
      fill="none"
      fillRule="evenodd"
      stroke="#4A4A4A"
      strokeWidth="2"
      d="M5 9l6.911 6.911L19.208 9"
    />
  </svg>
);
