import React from "react";
import { CSSTransition } from "react-transition-group";
import classnames from "class-names";

const Tooltip = ({ visible, children, className, placement = "bottom" }) => (
  <CSSTransition
    in={visible}
    timeout={300}
    unmountOnExit
    classNames={"popup-animation"}
  >
    <div
      className={classnames(
        "tooltip",
        className,
        "tooltip_placement_" + placement
      )}
    >
      {children}
    </div>
  </CSSTransition>
);

export default Tooltip;
