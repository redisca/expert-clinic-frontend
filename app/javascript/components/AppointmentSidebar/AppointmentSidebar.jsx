import React from "react";
import Sticky from "react-stickynode";

const AppointmentSidebar = ({ children }) => {
  return (
    <Sticky top={30} bottomBoundary=".appointment-form__sticky-boundary">
      {children}
    </Sticky>
  );
};

export default AppointmentSidebar;
