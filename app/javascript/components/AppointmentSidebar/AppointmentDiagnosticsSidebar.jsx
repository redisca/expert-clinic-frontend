import React from "react";
import Sidebar from "./AppointmentSidebar";

const AppointmentDiagnosticsSidebar = ({ successUrl }) => (
  <Sidebar>
    <div className="appointment-info-card">
      <div className="appointment-info-card__content">
        <div className="appointment-info-card__section">
          <div className="appointment-info-card__header appointment-info-card__header_indent-b_normal">
            Услуга
          </div>
          <div className="appointment-info-card__label appointment-info-card__label_price">
            МРТ головного мозга
            <span>2 500 ₽</span>
          </div>
        </div>
        <div className="appointment-info-card__section">
          <div className="appointment-info-card__header appointment-info-card__header_indent-b_normal">
            Клиника
          </div>
          <div className="appointment-info-card__label">
            ул. Арсения Степанова, д.2а
          </div>
        </div>
        <div className="appointment-info-card__section">
          <div className="appointment-info-card__header appointment-info-card__header_indent-b_normal">
            Желаемая дата посещения
          </div>
          <div className="appointment-info-card__label">24.11.18, 10:30</div>
        </div>
      </div>
      <div className="appointment-info-card__footer">
        <div className="appointment-info-card__header">Итоговая стоимость</div>
        <div className="appointment-info-card__price">
          2 375
          <span>₽</span>
          <div className="appointment-info-card__price-old">
            2 500
            <span>₽</span>
          </div>
        </div>
        <a className="button button_large button_full-width" href={successUrl}>
          Оставить заявку
        </a>
      </div>
    </div>
    <div className="appointment-form__note appointment-form__note_indent-t_large appointment-form__note_sidebar">
      Нажав «Записаться» вы соглашаетесь с условиями использования вебсайта
      «Эксперт»
    </div>
  </Sidebar>
);

export default AppointmentDiagnosticsSidebar;
