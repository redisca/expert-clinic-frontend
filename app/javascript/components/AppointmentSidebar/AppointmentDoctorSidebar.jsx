import React from "react";
import Sidebar from "./AppointmentSidebar";

const AppointmentDoctorSidebar = ({ successUrl, photoUrl }) => (
  <Sidebar>
    <div className="appointment-info-card">
      <div className="appointment-info-card__content">
        <div className="appointment-info-card__section">
          <div className="appointment-info-card__header appointment-info-card__header_indent-b_normal">
            Врач
          </div>
          <div className="appointment-info-card__doctor-info">
            <div className="appointment-info-card__doctor-photo">
              <img src={photoUrl} />
            </div>
            <div className="appointment-info-card__doctor-name">
              Шелест
              <span>Петр Викторович</span>
            </div>
          </div>
        </div>
        <div className="appointment-info-card__section">
          <div className="appointment-info-card__header appointment-info-card__header_indent-b_normal">
            Услуга
          </div>
          <div className="appointment-info-card__label">Первичный прием</div>
        </div>
        <div className="appointment-info-card__section">
          <div className="appointment-info-card__header appointment-info-card__header_indent-b_normal">
            Клиника
          </div>
          <div className="appointment-info-card__label">
            ул. Арсения Степанова, д.2а
          </div>
        </div>
        <div className="appointment-info-card__section">
          <div className="appointment-info-card__header appointment-info-card__header_indent-b_normal">
            Желаемая дата посещения
          </div>
          <div className="appointment-info-card__label">Не выбрано</div>
        </div>
      </div>
      <div className="appointment-info-card__footer">
        <div className="appointment-info-card__header">Итоговая стоимость</div>
        <div className="appointment-info-card__price">
          1 140
          <span>₽</span>
          <div className="appointment-info-card__price-old">
            1 200
            <span>₽</span>
          </div>
        </div>
        <a href={successUrl} className="button button_large button_full-width">
          Оставить заявку
        </a>
      </div>
    </div>
    <div className="appointment-form__note appointment-form__note_indent-t_large">
      Нажав «Записаться» вы соглашаетесь с условиями использования вебсайта
      «Эксперт»
    </div>
  </Sidebar>
);

export default AppointmentDoctorSidebar;
