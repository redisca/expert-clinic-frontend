import React from "react";
import classnames from "class-names";

class MobileSearch extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: "",
      active: false
    };

    this.closeSearch = this.closeSearch.bind(this);
    this.openSearch = this.openSearch.bind(this);
  }

  closeSearch() {
    this.setState({ value: "", active: false });
  }

  openSearch() {
    this.setState({ active: true });
  }

  render() {
    const { value, active } = this.state;

    return (
      <div
        className={classnames("mobile-search", {
          "mobile-search_opened": active
        })}
      >
        <input
          className="mobile-search__input"
          value={value}
          placeholder="Поиск"
          onChange={event => this.setState({ value: event.target.value })}
          onBlur={this.closeSearch}
          onFocus={this.openSearch}
        />
        {active && (
          <div className="mobile-search__close" onClick={this.closeSearch} />
        )}
      </div>
    );
  }
}

export default MobileSearch;
