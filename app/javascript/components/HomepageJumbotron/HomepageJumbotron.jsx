import React from "react";
import classnames from "class-names";
import Plx from "react-plx";

class HomepageJumbotron extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentSlide: 0,
      inited: false
    };

    this.currentSlideRef = React.createRef();
    this.timer = null;

    this.changeSlide = this.changeSlide.bind(this);
    this.nextSlide = this.nextSlide.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
  }

  componentDidMount() {
    // Prevents animation flickering on first load
    setTimeout(() => this.setState({ inited: true }), 200);

    if (typeof window !== "undefined") {
      this.resetTimer();
    }
  }

  componentWillUnmount() {
    if (typeof window !== "undefined") {
      clearInterval(this.timer);
    }
  }

  changeSlide(currentSlide) {
    this.resetTimer();
    this.setState({ currentSlide });
  }

  nextSlide() {
    const { currentSlide } = this.state;
    const { slides } = this.props;
    this.changeSlide((currentSlide + 1) % slides.length);
  }

  resetTimer() {
    if (this.timer) {
      clearInterval(this.timer);
    }
    this.timer = setInterval(this.nextSlide, this.props.tick);
  }

  render() {
    const { currentSlide, inited } = this.state;
    const { slides, background } = this.props;

    return (
      <div className="jumbotron-slider">
        <Plx
          animateWhenNotInViewport
          parallaxData={[
            {
              start: "self",
              duration: "100vh",
              properties: [
                {
                  property: "translateY",
                  startValue: -120,
                  endValue: 0
                }
              ]
            }
          ]}
          className="jumbotron-slider__image"
        >
          <div className="jumbotron-slider__image-element" />
        </Plx>

        <div className="container">
          <div className="jumbotron-slider-slides">
            {slides.map((slide, index) => (
              <div
                className={classnames("jumbotron-slider__container", {
                  "jumbotron-slider__container_hidden": currentSlide !== index,
                  "jumbotron-slider__container_no-transition": !inited
                })}
                ref={currentSlide === index ? this.currentSlideRef : null}
                key={index}
              >
                <p
                  className="jumbotron-slider__title"
                  dangerouslySetInnerHTML={{ __html: slide.title }}
                />
                <p className="jumbotron-slider__description">
                  {slide.description}
                </p>
              </div>
            ))}
          </div>
          <div className="jumbotron-slider__dots">
            {slides.map((_, index) => (
              <div
                className={classnames("jumbotron-slider__dot", {
                  "jumbotron-slider__dot_active": currentSlide === index
                })}
                key={index}
                onClick={() => this.changeSlide(index)}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default HomepageJumbotron;
