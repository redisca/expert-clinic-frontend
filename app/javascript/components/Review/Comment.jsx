import React, { useState, useRef, useEffect } from "react";
import classnames from "class-names";

const CommentNew = ({ date, name, text, doctor }) => {
  const [expandable, setExpandable] = useState(false);
  const [expanded, setExpanded] = useState(false);
  const [expandedHeight, setExpandedHeight] = useState(null);

  const textRef = useRef(null);

  const adjustExpandable = () => {
    if (textRef) {
      const height = textRef.current.scrollHeight;
      const visibleHeight = textRef.current.clientHeight;

      if (visibleHeight < height) {
        setExpandable(true);
        setExpandedHeight(height);
      }
    }
  };

  useEffect(() => {
    setTimeout(adjustExpandable, 200);

    if (typeof window !== "undefined") {
      window.addEventListener("resize", adjustExpandable);

      return () => {
        window.removeEventListener("resize", adjustExpandable);
      };
    }
  });

  return (
    <div className="reviews-review">
      <div
        className={classnames("reviews-review__content-wrapper", {
          "reviews-review__content-wrapper_expanded": expanded || !expandable
        })}
      >
        <div
          className="reviews-review__content"
          style={{ maxHeight: expanded && expandedHeight }}
          ref={textRef}
        >
          {text}
        </div>
        {expandable && (
          <div
            className={classnames("reviews-review__more", {
              "reviews-review__more_hidden": expanded
            })}
            onClick={() => {
              setExpanded(true);
            }}
          >
            Читать дальше
          </div>
        )}
      </div>

      <div className="reviews-review__info">
        <div className="reviews-review__name">{name}</div>
        <div className="reviews-review__doctor">
          о специалисте&nbsp;
          <span>{doctor}</span>
        </div>
        <div className="reviews-review__date">{date}</div>
      </div>
    </div>
  );
};

class Comment extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      expandable: false,
      expanded: false,
      expandedHeight: null
    };

    this.textRef = React.createRef();

    this.adjustExpandable = this.adjustExpandable.bind(this);
  }

  componentDidMount() {
    setTimeout(this.adjustExpandable, 200);

    if (typeof window !== "undefined") {
      window.addEventListener("resize", this.adjustExpandable);
    }
  }

  componentWillUnmount() {
    if (typeof window !== "undefined") {
      window.removeEventListener("resize", this.adjustExpandable);
    }
  }

  adjustExpandable() {
    if (this.textRef) {
      const height = this.textRef.current.scrollHeight;
      const visibleHeight = this.textRef.current.clientHeight;

      if (visibleHeight < height) {
        this.setState({ expandable: true, expandedHeight: height });
      }
    }
  }

  render() {
    const { date, name, text } = this.props;
    const { expandable, expanded, expandedHeight } = this.state;

    return (
      <div className="reviews-review">
        <div className="reviews-review__date">{date}</div>
        <div className="reviews-review__name">{name}</div>
        <p
          className={classnames("reviews-review__content", {
            "reviews-review__content_expanded": expanded
          })}
          style={{ maxHeight: expanded && expandedHeight }}
          ref={this.textRef}
        >
          {text}
        </p>
        {expandable && (
          <div
            className={classnames("reviews-review__more", {
              "reviews-review__more_hidden": expanded
            })}
            onClick={() => this.setState({ expanded: true })}
          >
            Читать дальше
          </div>
        )}
      </div>
    );
  }
}

export default CommentNew;
