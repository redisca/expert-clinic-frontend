import React from "react";
import SpecializationFilter from "../FiltersRow/SpecializationFilter";
import DropdownFilter from "../Filter/DropdownFilter";

const doctors = {
  0: "Все врачи",
  1: "Шелест Петр Викторович",
  2: "Наседкина Галина Григорьевна",
  3: "Пермяков Сергей Владимирович",
  4: "Ахлакова Патимат Магомедовна",
  5: "Анненков Андрей Викторович"
};

const ReviewsFilters = () => (
  <div className="filters reviews-page__filters">
    <div className="reviews-page__filter">
      <SpecializationFilter />
    </div>
    <div className="reviews-page__filter">
      <DropdownFilter
        name="time"
        getTitle={value => doctors[value]}
        title="Врач"
        values={doctors}
        defaultValue={0}
      />
    </div>
  </div>
);

export default ReviewsFilters;
