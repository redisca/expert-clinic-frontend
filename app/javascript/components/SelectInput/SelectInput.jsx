import React, { useState, useRef } from "react";
import CustomScroll from "react-custom-scroll";
import { CSSTransition } from "react-transition-group";
import { useScrollEventListener } from "../../hooks/useScrollEventListener";
import { useOutsideClick } from "../../hooks/useOutsideClick";
import classnames from "class-names";

const SelectInput = ({ options, defaultValue }) => {
  const [dropdownShown, setDropdownShown] = useState(false);
  const [value, setValue] = useState(defaultValue);
  const container = useRef(null);

  useScrollEventListener(() => setDropdownShown(false));
  useOutsideClick(container, () => setDropdownShown(false));

  const clickItem = newValue => () => {
    setValue(newValue);
    setDropdownShown(false);
  };

  return (
    <div style={{ position: "relative" }} ref={container}>
      <div
        className={classnames(
          "input-group__input input-group__input_dropdown",
          { "input-group__input_expanded": dropdownShown }
        )}
        onClick={() => setDropdownShown(!dropdownShown)}
      >
        <span>{value && options && options[value]}</span>
      </div>
      <CSSTransition
        in={dropdownShown}
        timeout={300}
        unmountOnExit
        classNames={"filter-dropdown-animation"}
      >
        <div className="input-group-dropdown">
          <CustomScroll>
            <div className="input-group-dropdown-inner">
              {Object.keys(options).map(key => (
                <div
                  className="input-group-dropdown__item"
                  key={key}
                  onClick={clickItem(key)}
                >
                  {options[key]}
                </div>
              ))}
            </div>
          </CustomScroll>
        </div>
      </CSSTransition>
    </div>
  );
};

export default SelectInput;
