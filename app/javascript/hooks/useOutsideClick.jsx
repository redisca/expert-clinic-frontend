import React, { useEffect } from "react";

export function useOutsideClick(ref, handler) {
  const clickHandler = event => {
    if (ref && ref.current && !ref.current.contains(event.target)) {
      handler(event);
    }
  };

  useEffect(() => {
    document.addEventListener("click", clickHandler);

    return () => {
      document.removeEventListener("click", clickHandler);
    };
  });
}
