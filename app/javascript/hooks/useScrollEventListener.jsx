import React, { useEffect } from "react";

export function useScrollEventListener(handler, target = window) {
  useEffect(() => {
    if (target) {
      target.addEventListener("scroll", handler);

      return () => {
        target.removeEventListener("scroll", handler);
      };
    }
  });
}
