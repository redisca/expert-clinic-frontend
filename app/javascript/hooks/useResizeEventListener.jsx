import React, { useEffect } from "react";

export function useResizeEventListener(handler, target = window) {
  useEffect(() => {
    if (target) {
      target.addEventListener("resize", handler);

      return () => {
        target.removeEventListener("resize", handler);
      };
    }
  });
}
