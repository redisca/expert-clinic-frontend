Rails.application.routes.draw do
  root 'frontend#homepage'
  get '/doctors', :to => 'frontend#doctors'
  get '/doctors/view', :to => 'frontend#doctor_page'

  get '/services/adults', :to => 'frontend#services_adults'
  get '/services/mri', :to => 'frontend#service_mri'
  get '/services/cardiology', :to => 'frontend#service_cardiology'
  get '/services/diagnostics', :to => 'frontend#services_diagnostics'
  get '/services/surgery', :to => 'frontend#services_surgery'
  get '/services/page', :to => 'frontend#service_page'

  get '/about/clients', :to => 'frontend#about_clients'
  get '/about/partners', :to => 'frontend#about_partners'
  get '/about/investors', :to => 'frontend#about_investors'
  get '/about/legal', :to => 'frontend#about_legal'
  get '/about/jobs', :to => 'frontend#about_jobs'

  get '/patients/notes', :to => 'frontend#patients_notes'
  get '/patients/faq', :to => 'frontend#patients_faq'
  get '/patients/legal', :to => 'frontend#patients_legal'

  get '/promos', :to => 'frontend#promos'
  get '/promos/view', :to => 'frontend#promo_view'

  get '/appointment/doctor', :to => 'frontend#appointment_doctor'
  get '/appointment/doctor/success', :to => 'frontend#appointment_doctor_success'
  get '/appointment/diagnostics', :to => 'frontend#appointment_diagnostics'
  get '/appointment/diagnostics/success', :to => 'frontend#appointment_diagnostics_success'

  get '/reviews', :to => 'frontend#reviews'

  get '/blog', :to => 'frontend#blog'
  get '/blog/view', :to => 'frontend#blog_view'
  get '/blog/author', :to => 'frontend#blog_author'

  get '_/city-confirm', :to => 'frontend#city_confirm'

  get 'user/login', :to => 'user#login'
  get 'user/signup', :to => 'user#signup'
  get 'user/signup/sms', :to => 'user#signup_sms'
  get 'user/signup/sms/resent', :to => 'user#signup_sms_resent'
  get 'user/signup/password', :to => 'user#signup_password'
  get 'user/signup/success', :to => 'user#signup_success'
  get 'user/reset_password', :to => 'user#reset_password'
  get 'user/reset_password', :to => 'user#reset_password'
  get 'user/reset_password/sms', :to => 'user#reset_password_sms'
  get 'user/reset_password/sms/resent', :to => 'user#reset_password_sms_resent'
  get 'user/reset_password/change', :to => 'user#reset_password_change'
  get 'user/reset_password/success', :to => 'user#reset_password_success'

  get 'cabinet/appointments', :to => 'user#cabinet_appointments'
  get 'cabinet/doctors', :to => 'user#cabinet_doctors'
  get 'cabinet/profile', :to => 'user#cabinet_profile'
  get 'cabinet/offers', :to => 'user#cabinet_offers'
end
